# -*- mode: python; python-indent: 4 -*-
import time

import ncs
from ncs.application import Service


class Increment(ncs.dp.Action):
    """Increment the dummy leaf on our list instance
    """
    @ncs.dp.Action.action
    def cb_action(self, uinfo, name, kp, action_input, action_output):
        with ncs.maapi.single_write_trans('pua-tester', 'system',
                                          db=ncs.OPERATIONAL) as t_write:
            service = ncs.maagic.get_node(t_write, kp)
            time.sleep(service.sleep_duration)
            service.dummy += 1
            t_write.apply()

class ServiceCallbacks(Service):
    @Service.create
    def cb_create(self, tctx, root, service, proplist):
        self.log.info(f"Service create(service='{service._path}'), sleeping for {service.create_slowness}")
        time.sleep(float(service.create_slowness))


class Main(ncs.application.Application):
    def setup(self):
        self.register_service('slowness-servicepoint', ServiceCallbacks)
        #self.register_action('pua-tester-increment', Increment)
