# -*- mode: python; python-indent: 4 -*-
import logging
import select
import socket

import ncs
from _ncs import events
import _ncs

from bgworker import background_process

from opentelemetry import trace
from opentelemetry.ext import jaeger
from opentelemetry.sdk.trace import TracerProvider
from opentelemetry.sdk.trace.export import SimpleExportSpanProcessor

from . import ptrace

def OpenTelemetryExporter():
    """Function for running as NSO background worker to subscribe to NSO
    progress-trace notification and export to tracing system
    """
    log = logging.getLogger('opentelemetry-exporter')
    log.info('OpenTelemetry progress-trace exporter started')

    # Read in configuration from CDB
    with ncs.maapi.single_read_trans('opentelemetry-exporter', 'system') as t:
        root = ncs.maagic.get_root(t)
        ot_config = root.progress.opentracing
        cfg_reporting_host = ot_config.reporting_host
        cfg_reporting_port = ot_config.reporting_port
        cfg_logging = ot_config.logging

    jaeger_exporter = jaeger.JaegerSpanExporter(
        service_name="NSO",
        agent_host_name=cfg_reporting_host,
        agent_port=cfg_reporting_port,
    )

    trace.set_tracer_provider(TracerProvider())
    trace.get_tracer_provider().add_span_processor(
        SimpleExportSpanProcessor(jaeger_exporter)
    )

    tracer = trace.get_tracer('opentelemetry-exporter')

    ptrace.process_traces(tracer, get_ptrace_notifs())


def get_ptrace_notifs():
    """Generator that yields NSO progress-trace events, coming from the NSO
    notification API that we subscribed to
    """
    # set up listener for NSO notifications
    event_sock = socket.socket()
    mask = events.NOTIF_PROGRESS
    noexists = _ncs.Value(init=1, type=_ncs.C_NOEXISTS)
    notif_data = _ncs.events.NotificationsData(heartbeat_interval=1000, health_check_interval=1000, stream_name='whatever', start_time=noexists, stop_time=noexists, verbosity=ncs.VERBOSITY_VERY_VERBOSE)
    events.notifications_connect2(event_sock, mask, ip='127.0.0.1', port=ncs.NCS_PORT, data=notif_data)

    while True:
        (readables, _, _) = select.select([event_sock], [], [])
        for readable in readables:
            if readable == event_sock:
                event_dict = events.read_notification(event_sock)
                pev = event_dict['progress']
                yield pev


class Main(ncs.application.Application):
    worker = None

    def setup(self):
        self.worker = background_process.Process(self, OpenTelemetryExporter, config_path='/progress:progress/opentracing-exporter:opentracing/enabled')
        self.worker.start()

    def teardown(self):
        self.worker.stop()
